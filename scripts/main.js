import React from 'react';
import ReactDOM from 'react-dom';
import Perf from 'react-addons-perf';
import Tactics from './components/Tactics';

window.Perf = Perf;

ReactDOM.render(<Tactics/>, document.querySelector('#main'));
