import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import h from '../lib/helpers';
import Immutable from 'immutable';

// Import our own components
import Finished from '../components/Finished';
import TacticsGame from '../components/TacticsGame';
import Header from '../components/Header';

/*
  Tactics (main app)
  This will let us make <Tactics/>
*/

const Tactics = React.createClass({
    mixins: [PureRenderMixin],

    /* No PropTypes because this is the main app which will be based on the State */

    getInitialState: function() {

        var initData = {
            startTurn: true,
            gameTurn: true,
            gameFinished: false,
            player1: h.createPlayer('Player 1'),
            player2: h.createPlayer('Player 2'),
        };

        return {
            data: Immutable.fromJS(initData)
        };
    },

    changeTurn: function() {
        this.setState(({data}) => ({
            data: data.update('gameTurn', turn => !turn)
        }));
    },

    hitNumber: function(number, active) {
        if (active) {

            this.setState(function(prevState) {
                var data = prevState.data;
                var player = h.getPlayer(data, 'active');
                if (player.getIn(['score', number.toString(), 'hits']) < 3) {

                    // Update state based on these booleans
                    var activePlayerKey = h.getPlayerKey(data, 'active');
                    var otherPlayerKey = h.getPlayerKey(data, 'other');

                    // Create newData, the Immutable Map that holds the new state, then fill it.
                    var newData;

                    // Find out if the game is finished with this hit
                    var gameFinished = h.gameFinished(data, number);
                    if (gameFinished.finished) {
                        newData = h.updateWinner(data, gameFinished, number);
                    } else if (player.getIn(['score', number.toString(), 'hits']) === 2) {
                        newData = data
                        .updateIn([activePlayerKey, 'score', number.toString(), 'hits'], hits => hits + 1)
                        .updateIn([otherPlayerKey, 'score', number.toString(), 'completed'], value => !value);
                    } else {
                        newData = data.updateIn([activePlayerKey, 'score', number.toString(), 'hits'], hits => hits + 1)
                    }

                    return {
                        data: newData
                    }
                } else {
                    // state not to be changed
                    return prevState;
                }
            });

        }
    },

    addPoints: function(number) {
        var data = this.state.data;
        var gameFinished = h.gameFinished(data, number);
        if (gameFinished.finished) {
            this.setState(({data}) => ({
                data: h.updateWinner(data, gameFinished, number)
            }));
        } else {
            this.setState(({data}) => ({
                data: data.updateIn([h.getPlayerKey(data, 'active'), 'score', number.toString(), 'hits'], hits => hits + 1)
            }));
        }

    },

    // reset state for a new game
    resetGame: function() {

        this.setState(({data}) => (
        {
            data: data
                .update('startTurn', value => !value)
                .update('gameTurn', value => !data.get('startTurn'))
                .update('gameFinished', value => !value)
                .updateIn(['player1', 'score'], score => Immutable.fromJS(h.getCleanScore()))
                .updateIn(['player2', 'score'], score => Immutable.fromJS(h.getCleanScore()))
        }));
    },

    render: function() {
        var data = this.state.data;

        // Figure out if game is finished
        var finished = '';
        if (data.get('gameFinished')) {
            finished = <Finished player1={data.get('player1')} player2={data.get('player2')} resetGame={this.resetGame}/>;
        }

        return (
            <div className="tactics">
                <Header tagline="Tactics"/>
                <TacticsGame player1={data.get('player1')} player2={data.get('player2')} turn={data.get('gameTurn')} changeTurn={this.changeTurn} hitNumber={this.hitNumber} addPoints={this.addPoints}/>
                {finished}
            </div>
        )
    }
});

export default Tactics;
