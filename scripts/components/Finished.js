import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import h from '../lib/helpers';

/*
    Finished component: small overlay popup when game is finished
    <Finished />
 */

const Finished = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        player1: React.PropTypes.object.isRequired,
        player2: React.PropTypes.object.isRequired,
        resetGame: React.PropTypes.func.isRequired
    },

    render: function() {
        var points1 = h.calculateScore(this.props.player1.get('score'));
        var points2 = h.calculateScore(this.props.player2.get('score'));

        return (
            <div className="overlay-container">

                    <div className="popup">
                        <h2>Game shot</h2>
                        <div className="popup-standings">
                            <div className="row">
                                <div className="col-md-4 col-sm-4">{this.props.player1.get('name')}</div>
                                <div className="col-md-4 col-sm-4 score">{points1}</div>
                                <div className="col-md-4 col-sm-4 rounds-won">{this.props.player1.get('roundsWon')}</div>
                            </div>
                            <div className="row">
                                <div className="col-md-4 col-sm-4">{this.props.player2.get('name')}</div>
                                <div className="col-md-4 col-sm-4 score">{points2}</div>
                                <div className="col-md-4 col-sm-4 rounds-won">{this.props.player2.get('roundsWon')}</div>
                            </div>
                        </div>

                        <div className="popup-buttons">
                            <button type="button" className="btn btn-warning" onClick={this.props.resetGame}><span className="glyphicon glyphicon-play-circle"></span> Play again</button>
                            <button type="button" className="btn btn-default"><span className="glyphicon glyphicon-menu-hamburger"></span> Menu</button>
                        </div>
                    </div>

            </div>
        )
    }
});

export default Finished;
