import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

/*
    Standings component: small container for standings in the game
    <Standings />
 */

const Standings = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        wonPlayer1: React.PropTypes.number.isRequired,
        wonPlayer2: React.PropTypes.number.isRequired
    },

    render: function() {
        return (
            <div className="standings">
                <h2>{this.props.wonPlayer1} - {this.props.wonPlayer2}</h2>
            </div>
        )
    }
});

export default Standings;
