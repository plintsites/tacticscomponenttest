import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import Row from '../components/Row';
import test from 'tape';

export function testRow() {
    test('Row component', (t) => {
        // SIMPLE TESTS
        // 1] Test that for a given number of hits the .row-items have the correct class
        // 2] Test that for a 3 or more hits the .row is complete
        // 3] Test that the Row component contains a <PointsCounter />
        // 4] Clicking a .row-item will execute the callback function

        const number=18, hits=2, completed=true, turn=true, amount=0, status='';
        const addPoints = () => {};
        const hitNumber = () => {};

        const result = shallow(<Row
            number={number}
            hits={hits}
            completedByOpponent={completed}
            turn={turn}
            hitNumber={hitNumber}
            addPoints={addPoints}
        />);

        t.test('-- With two hits, the row-items have a class hit-2', (t) => {
            t.plan(1);
            t.ok(result.find('.row-item').first().hasClass('hit-2'));
        });

        t.test('-- The Row component contains a PointsCounter component', (t) => {
            t.plan(1);

            const result = mount(<Row
                number={number}
                hits={hits}
                completedByOpponent={completed}
                turn={turn}
                hitNumber={hitNumber}
                addPoints={addPoints}
            />);

            t.ok(result.find('.points-container').hasClass('hidden'));
        });

        t.test('-- With three hits, the row is completed', (t) => {
            t.plan(1);

            const hits = 3;

            const result = shallow(<Row
                number={number}
                hits={hits}
                completedByOpponent={completed}
                turn={turn}
                hitNumber={hitNumber}
                addPoints={addPoints}
            />);

            t.ok(result.hasClass('row-completed'));
        });

        t.test('-- Clicking a row-item calls the callback', (t) => {
            t.plan(1);

            const hitNumber = function() {
                t.ok(15 > 3);
            };

            const result = mount(<Row
                number={number}
                hits={hits}
                completedByOpponent={completed}
                turn={turn}
                hitNumber={hitNumber}
                addPoints={addPoints}
            />);

            result.find('.row-item').first().simulate('click');
        });

    });

}

