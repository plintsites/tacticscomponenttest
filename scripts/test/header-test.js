import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import Header from '../components/Header';
import test from 'tape';

export function testHeader() {
    test('Header component', (t) => {
        t.test('-- The active menu icon is Home', (t) => {
            t.plan(1);

            const tagline = 'test';
            const header = shallow(<Header tagline={tagline}/>);

            t.equal(header.find('.nav.navbar-nav .active').text(), 'Home');
        });
    });
}

