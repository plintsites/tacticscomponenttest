import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import Standings from '../components/Standings';
import test from 'tape';

export function testStandings() {
    test('Standings component', (t) => {
        t.test('-- The current standings is 3-2', (t) => {
            t.plan(1);

            const p1 = 3;
            const p2 = 2;
            const result = shallow(<Standings wonPlayer1={p1} wonPlayer2={p2}/>);

            t.equal(result.find('h2').text(), '3 - 2');
        });
    });
}

